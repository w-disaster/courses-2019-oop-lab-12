﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;
        public Map2D()
        {
            this.values = new Dictionary<Tuple<TKey1,TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach(TKey1 tk1 in keys1)
            {
                foreach(TKey2 tk2 in keys2)
                {
                    this.values.Add(new Tuple<TKey1,TKey2>(tk1,tk2),generator.Invoke(tk1, tk2));
                }
            }
            //throw new NotImplementedException();
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            /* Description:
            * foreach TKey1 value in entry variable create a list from other which single elements are a tuple with
            * column (Tkey2) value and the element (TValue) value. If the list is not empty check if it
            * is contains the other values of entry */

            IList<Tuple<TKey2, TValue>> row;
           
            foreach (var entry in this.values)
            {
                if((row = other.GetRow(entry.Key.Item1)).Count > 0)
                {
                    if( !row.Contains(new Tuple<TKey2, TValue> (entry.Key.Item2, entry.Value)))
                    {
                        return false;
                    }

                }else
                {
                    return false;
                }
            }
            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                return this.values[new Tuple<TKey1, TKey2>(key1, key2)];
            }

            set
            {
                this.values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> row = new System.Collections.Generic.List<Tuple<TKey2, TValue>>();
            foreach(var tk in this.values)
            {
                if (tk.Key.Item1.Equals(key1))
                {
                    row.Add(new Tuple<TKey2, TValue>(tk.Key.Item2, tk.Value));
                }
            }
            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> column = new System.Collections.Generic.List<Tuple<TKey1, TValue>>();
            foreach (var entry in this.values)
            {

                if (entry.Key.Item2.Equals(key2))
                {
                    column.Add(new Tuple<TKey1, TValue>(entry.Key.Item1, entry.Value));
                }
            }
            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> elements = new System.Collections.Generic.List<Tuple<TKey1, TKey2, TValue>>() ;
            foreach (var entry in this.values)
            {
                elements.Add(new Tuple<TKey1, TKey2, TValue>(entry.Key.Item1, entry.Key.Item2, entry.Value));
            }
            return elements;
        }

        public int NumberOfElements
        {
            get
            {
                return this.GetElements().Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            if(obj.GetType().Equals(this.GetType()))
            {
                return this.Equals(obj);
            }
            Console.WriteLine("object class from parameter is not equal to the dictionary's");
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
